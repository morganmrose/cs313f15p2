1. TODO also try with a LinkedList - does it make any difference? Does not make any difference on whether or not the code will run - both work and pass all tests.

2.	public void setUp() throws Exception {
		list = new ArrayList<Integer>();
		//list= new LinkedList<Integer>();
		// TODO also try with a LinkedList - does it make any difference? No, no difference, both run without errors and pass the tests.

3. TODO what happens if you use list.remove(77)?
    Will give an out of bounds error - index 77 does not exist


Performance Tests:

When size is 10:
testLinkedListAddRemove: 20 ms
testArrayListAddRemove: 43 ms
testLinkedListAccess: 32 ms
testArrayListAccess: 90 ms

When size is 100:
testLinkedListAddRemove: 51 ms
testArrayListAddRemove: 52 ms
testLinkedListAccess: 24 ms
testArrayListAccess: 102 ms

When size is 1000:
testLinkedListAddRemove: 16 ms
testArrayListAddRemove: 44 ms
testLinkedListAccess: 491 ms
testArrayListAccess: 542 ms

When size is 10000:
testLinkedListAddRemove: 24 ms
testArrayListAddRemove: 45 ms
testLinkedListAccess: 7363 ms
testArrayListAccess: 4071 ms